#　使用electron进行跨端应用开发的一些心得

1. yarn比npm安装依赖更快

yarn可以解释npm的package.json，安装依赖，一些命令对比：

```bash
npm install -g xx
yarn global add xx

npm install
yarn

npm run start
yarn run start
```

2. 可以在Linux下同时构建三站的app

windows, macos, linux三个平台的app都可以在linux下构建。所以用CI, github actions就行了。

使用webpack打包应用的命令如下：

```yaml
    - electron-builder build --linux deb -c ./electron-builder-config.js
    - electron-builder build --linux AppImage -c ./electron-builder-config.js
    - electron-builder build --mac tar.gz --x64 -c ./electron-builder-config.js
    - electron-builder build --win --x64 -c ./electron-builder-config.js
```

所有能构建的变种可以在https://www.electron.build/configuration/mac查询。
其中mac os平台在linux的CI容器中只能构建tar.gz格式的, pkg和dmg格式的都会因为某种原因不成功, dmg是因为没有相应的sign可执行程序, pkg理论上有可能成功，在我的平台上是go语言崩溃了，原因不明。

我构建的时候使用容器镜像是：electronuserland/builder:wine ，但是似乎不用它也能构建windows镜像，存疑。

最好不要直接使用这个镜像，而是在这个镜像里先执行一次构建，三端的构建都要执行，然后再保存一个容器，使用保存的新容器来做CI。因为这样有缓存，构建速度更快。

![](https://vscode-remote+bronze-002ddingo-002d70go2uep-002ews-002dus23-002egitpod-002eio.vscode-resource.gitpod.io/workspace/GitNote/assets/20211216_121349_image.png)

3. npm run命令

npm run命令执行的是package.json中scripts段中对应的命令。
比如说npm run build，它会执行package.json中script下的prebuild, build, postbuild三个命令依次执行。
所有的用户自定义命令都会在之前和之后执行pre, post前缀的对应命令。

npm run执行命令的方法是先启动一个shell, 把node_modules/.bin加入环境变量，然后执行package.json中的命令。所以devDependency只需要local安装就可以在scripts中使用。

参考：https://www.ruanyifeng.com/blog/2016/10/npm_scripts.html

4. electron采用的是main和render两进程分离的架构

一个electron程序可以包含一个main process和多个render process。render process可以类比为谷歌浏览器的tab，它通过一个window启动，加载一个网页。
render process可以操作DOM。
main process不可以操作DOM，但是可以调用系统功能，如nodejs的fs之类的API。
main process还有启动一个server来serve　render rpocess所需的静态网页。

参考：
https://www.electronjs.org/docs/latest/tutorial/process-model
https://medium.com/jspoint/a-beginners-guide-to-creating-desktop-applications-using-electron-824da5665047

其实你也可以在main process里再起动别的process的吧？理论上是可以的，没试过。

5. GitLab有现成的机制同步代码(to and from)到github或其他平台

所以贪图github的广泛应用和GitLab的轻便WebIDE的人可以在GitLab上写代码，然后同步到GitHub上。

路径："Settings > Repository > Mirroring repositories"
